from functools import partial

from jax import jit, numpy as jnp
from jax.lax import map as jmap
from numpyro import enable_x64, sample, distributions as dist

from .single_fidelity_gp import bgp_model, bgp_predict_loop_1
from .models_utils import (
    mcmc,
    normalize_training_data,
    normalize,
    denormalize,
    householder_reparameterization,
)

enable_x64()


###################################
# Numpyro model used for training #
###################################


def bfs_model(training_data, num_active_dims, use_alternate_priors=False):
    # Problem dimensions
    m = num_active_dims
    n = training_data["x"].shape[1]
    num_projection_parameters = m * n - m * (m - 1) // 2

    # Priors
    projection_parameters = sample(
        "projection_parameters",
        dist.Normal(
            jnp.zeros((num_projection_parameters,)),
            jnp.ones((num_projection_parameters,)),
        ),
    )

    # Project inputs
    w = householder_reparameterization(projection_parameters, n, m)
    projected_training_data = {"x": training_data["x"] @ w, "y": training_data["y"]}
    return bgp_model(projected_training_data, use_alternate_priors=use_alternate_priors)


######################
# Prediction routine #
######################

# Body of the loop over the posterior draws (`posterior_draw` is one element)
def bfs_predict_inner(pred_x, training_data, num_active_dims, posterior_draw):
    # Problem dimensions
    m = num_active_dims
    n = training_data["x"].shape[1]

    # Retrieve projection matrix
    projection_parameters = posterior_draw["projection_parameters"]
    w = householder_reparameterization(projection_parameters, n, m)

    # Project predictions sites
    projected_pred_x = pred_x @ w

    projected_training_data = {"x": training_data["x"] @ w, "y": training_data["y"]}

    return bgp_predict_loop_1(projected_pred_x, projected_training_data, posterior_draw)


@partial(jit, static_argnums=(3))
def bfs_predict(pred_x, posterior_draws, training_data, num_active_dims):
    means, variances = jmap(
        partial(bfs_predict_inner, pred_x, training_data, num_active_dims),
        posterior_draws,
    )
    return means.T, variances.T


######################################################
# High-Level Training/Prediction/Validation Routines #
######################################################


def sf_ridge_gp_train(
    training_data,
    mcmc_parameters,
    dim_feature_space,
    mcmc_file_path=None,
    use_alternate_priors=False,
):
    # Normalize training data
    normalized_training_data, normalization_constants = normalize_training_data(
        training_data
    )

    # Sample from the posterior chains using MCMC
    posterior_draws, training_duration = mcmc(
        bfs_model,
        (normalized_training_data, dim_feature_space),
        {"use_alternate_priors": use_alternate_priors},
        mcmc_file_path=mcmc_file_path,
        **mcmc_parameters,
    )

    return {
        "dim_feature_space": dim_feature_space,
        "normalization_constants": normalization_constants,
        "posterior_draws": posterior_draws,
        "training_duration": training_duration,
    }


def sf_ridge_gp_predict(
    pred_x,
    training_data,
    training_artifacts,
):
    # Retrieve quantities of interest
    posterior_draws = training_artifacts["posterior_draws"]
    normalization_constants = training_artifacts["normalization_constants"]

    # Normalize the training data and the prediction sites
    norm_training_data = normalize_training_data(training_data, normalization_constants)
    norm_pred_x = normalize(
        pred_x,
        normalization_constants["x_offset"],
        normalization_constants["x_scaling"],
    )

    # Problem dimensions
    num_predictions = pred_x.shape[0]
    dim_feature_space = posterior_draws["length_scales"].shape[1]
    num_posterior_draws = posterior_draws["length_scales"].shape[0]

    # Prediction
    # print("norm_pred_x", norm_pred_x.shape)

    # print("\nposterior_draws")
    # for key, value in posterior_draws.items():
    #     print(key, value.shape)

    # print("\nnorm_training_data")
    # for key, value in norm_training_data.items():
    #     print(key, value.shape)

    # print(dim_feature_space)

    norm_means, norm_variances = bfs_predict(
        norm_pred_x, posterior_draws, norm_training_data, dim_feature_space
    )

    # Denormalization and flattening of all stochastic dimensions
    means = denormalize(
        norm_means,
        normalization_constants["y_offset"],
        normalization_constants["y_scaling"],
    ).reshape((num_predictions, num_posterior_draws))

    variances = (norm_variances * normalization_constants["y_scaling"] ** 2).reshape(
        (num_predictions, num_posterior_draws)
    )

    return means, variances
