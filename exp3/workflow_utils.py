import datetime
import enum
import itertools as it
import json
import os
from pathlib import Path
import random
import shutil
import subprocess
import time
from typing import Dict, List

import dill
import h5py as h5
import jax
from jax import jit, numpy as jnp
from jax.random import PRNGKey
from jax.scipy.special import logsumexp
import numpy as np
from numpyro import distributions as dist
import pandas as pd

from .multifidelity_deep_gp_ridge import (
    mf_deep_ridge_gp_train,
    mf_deep_ridge_gp_predict,
)
from .single_fidelity_ridge_gp import (
    sf_ridge_gp_train,
    sf_ridge_gp_predict,
)

###########################
# Configuration Constants #
###########################

DOE_NAME = os.environ["EXP3_DOE_NAME"]
DATA_DIR = Path(os.environ["EXP3_DATA_DIR"])
DATASETS_DIR = DATA_DIR / "datasets"
DOE_DIR = DATA_DIR / "does" / DOE_NAME
STATUS_DIR = DOE_DIR / "status"
TEMP_DIR = DOE_DIR / "temp"
RESULTS_DIR = DOE_DIR / "results"
FIGURES_DIR = DOE_DIR / "figures"
LOG_FILE = DOE_DIR / "workers.log"
PROCESS_PARAMETERS_FILE = DOE_DIR / "process_parameters.json"
CASES_FILE = DOE_DIR / "cases.csv"

#############################
# Main Case-Running Routine #
#############################


class CaseType(enum.Enum):
    HF_ONLY = 1
    LF_ONLY = 2
    MULTI_FIDELITY = 3


def run_case(case_number, worker_uid, testing=False):
    # Temporary folder for this specific case
    run_temp_dir = TEMP_DIR / "{}".format(case_number)
    run_temp_dir.mkdir(exist_ok=True)

    # Retrieve parameters
    process_parameters = get_process_parameters()
    case_parameters = get_case_parameters(int(case_number))

    # ⚠ TESTING
    if testing:
        process_parameters["training_parameters"]["mcmc_parameters"].update(
            {
                "num_warmup_draws": 5,
                "num_posterior_draws": 10,
                "num_draws_between_saves": 50,
                "progress_bar": True,
                "display_summary": True,
            }
        )

    # Step 1: prepare
    preparation_artifacts = prepare(run_temp_dir, case_parameters)

    # Step 2: train
    training_artifacts = train(
        run_temp_dir, case_parameters, process_parameters, preparation_artifacts
    )

    # Step 3: validate
    validation_artifacts = validate(
        run_temp_dir, process_parameters, preparation_artifacts, training_artifacts
    )

    # Step 4: save results
    save_results(
        case_number,
        worker_uid,
        case_parameters,
        process_parameters,
        preparation_artifacts,
        training_artifacts,
        validation_artifacts,
    )

    # Step 5: clean up the temporary directory
    clean_up_temp_dir(run_temp_dir)


def prepare(run_temp_dir, case_parameters):

    # Temporary file for the preparation artifacts
    temp_file_path = run_temp_dir / "preparation_artifacts.pkl"

    if temp_file_path.is_file():
        # If preparation already happened, just retrieve preparation artifacts from file
        with open(temp_file_path, "rb") as temp_file:
            preparation_artifacts = dill.load(temp_file)
    else:
        # Otherwise, we need to do the preparation

        # Retrieve parameters
        lf_dataset_name = case_parameters["lf_dataset_name"]
        hf_dataset_name = case_parameters["hf_dataset_name"]
        output_name = case_parameters["output_name"]
        num_lf_training_samples = case_parameters["num_lf_training_samples"]
        num_hf_training_samples = case_parameters["num_hf_training_samples"]
        lf_dataset_split_random_seed = case_parameters["lf_dataset_split_random_seed"]
        hf_dataset_split_random_seed = case_parameters["hf_dataset_split_random_seed"]

        # Determine the case type
        case_type = (
            CaseType.HF_ONLY
            if num_lf_training_samples == 0
            else CaseType.LF_ONLY
            if num_hf_training_samples == 0
            else CaseType.MULTI_FIDELITY
        )

        # Prepare LF data
        (
            lf_training_indices,
            lf_validation_indices,
        ) = prepare_training_validation_indices(
            lf_dataset_name,
            output_name,
            num_lf_training_samples,
            lf_dataset_split_random_seed,
        )
        (lf_training_data, lf_validation_data) = get_training_validation_sets(
            lf_dataset_name, output_name, lf_training_indices, lf_validation_indices
        )

        # Prepare HF data
        (
            hf_training_indices,
            hf_validation_indices,
        ) = prepare_training_validation_indices(
            hf_dataset_name,
            output_name,
            num_hf_training_samples,
            hf_dataset_split_random_seed,
        )
        (hf_training_data, hf_validation_data) = get_training_validation_sets(
            hf_dataset_name, output_name, hf_training_indices, hf_validation_indices
        )

        # Build preparation artifacts
        preparation_artifacts = {
            "case_type": case_type,
            "lf_training_indices": lf_training_indices,
            "lf_validation_indices": lf_validation_indices,
            "hf_training_indices": hf_training_indices,
            "hf_validation_indices": hf_validation_indices,
            "lf_training_data": lf_training_data,
            "lf_validation_data": lf_validation_data,
            "hf_training_data": hf_training_data,
            "hf_validation_data": hf_validation_data,
        }

        # Save preparation artifacts in the temp file
        with open(temp_file_path, "wb") as temp_file:
            dill.dump(preparation_artifacts, temp_file)

    return preparation_artifacts


def train(run_temp_dir, case_parameters, process_parameters, preparation_artifacts):
    # Temporary file for the training artifacts
    temp_file_path = run_temp_dir / "training_artifacts.pkl"

    if temp_file_path.is_file():
        # If training already happened, just retrieve training artifacts from file
        with open(temp_file_path, "rb") as temp_file:
            training_artifacts = dill.load(temp_file)
    else:
        # Training is not finished, we either need to resume or start from scratch

        # Load parameters
        case_type = preparation_artifacts["case_type"]
        lf_training_data = preparation_artifacts["lf_training_data"]
        hf_training_data = preparation_artifacts["hf_training_data"]
        training_parameters = process_parameters["training_parameters"]
        dim_feature_space = case_parameters["dim_feature_space"]
        feature_space_relation = case_parameters["feature_space_relation"]
        use_alternate_priors = (
            training_parameters["use_alternate_priors"]
            if "use_alternate_priors" in training_parameters
            else False
        )

        # File containing in-progress training data
        mcmc_file_path = run_temp_dir / "mcmc.pkl"

        # Start training from scratch
        if case_type == CaseType.HF_ONLY:
            training_artifacts = sf_ridge_gp_train(
                hf_training_data,
                training_parameters["mcmc_parameters"],
                dim_feature_space,
                mcmc_file_path=mcmc_file_path,
                use_alternate_priors=use_alternate_priors,
            )
        elif case_type == CaseType.LF_ONLY:
            training_artifacts = sf_ridge_gp_train(
                lf_training_data,
                training_parameters["mcmc_parameters"],
                dim_feature_space,
                mcmc_file_path=mcmc_file_path,
                use_alternate_priors=use_alternate_priors,
            )
        else:  # CaseType.MULTI_FIDELITY
            training_artifacts = mf_deep_ridge_gp_train(
                lf_training_data,
                hf_training_data,
                training_parameters["mcmc_parameters"],
                feature_space_relation,
                dim_feature_space,
                mcmc_file_path=mcmc_file_path,
                use_alternate_priors=use_alternate_priors,
            )

        # When done with training, we save the training artifacts
        with open(temp_file_path, "wb") as temp_file:
            dill.dump(training_artifacts, temp_file)

    return training_artifacts


def validate(
    run_temp_dir, process_parameters, preparation_artifacts, training_artifacts
):
    # # Temporary file for the preparation artifacts
    # temp_file_path = run_temp_dir / "validation_artifacts.pkl"

    # if temp_file_path.is_file():
    #     # If validation already happened, just retrieve validation artifacts from file
    #     with open(temp_file_path, "rb") as temp_file:
    #         validation_artifacts = dill.load(temp_file)
    # else:
    #     # Otherwise, we need to do the validation

    # Retrieve needed parameters
    case_type = preparation_artifacts["case_type"]
    lf_training_data = preparation_artifacts["lf_training_data"]
    lf_validation_data = preparation_artifacts["lf_validation_data"]
    hf_training_data = preparation_artifacts["hf_training_data"]
    hf_validation_data = preparation_artifacts["hf_validation_data"]
    validation_parameters = process_parameters["validation_parameters"]
    num_lf_predictions = validation_parameters["num_lf_predictions"]
    lf_predictions_seed = validation_parameters["lf_predictions_seed"]
    ci_bounds = validation_parameters["confidence_interval_bounds_cdf_values"]
    val_pred_rand_seed = validation_parameters["validation_predictions_random_seed"]
    num_gp_samples = validation_parameters["num_gp_samples"]

    def lf_predict(pred_x):
        if case_type == CaseType.LF_ONLY:
            return sf_ridge_gp_predict(
                pred_x,
                lf_training_data,
                training_artifacts,
            )
        elif case_type == CaseType.MULTI_FIDELITY:
            lf_means, lf_variances, _, _ = mf_deep_ridge_gp_predict(
                pred_x,
                lf_training_data,
                hf_training_data,
                training_artifacts,
                num_lf_predictions=num_lf_predictions,
                lf_predictions_seed=lf_predictions_seed,
            )
            return lf_means, lf_variances
        else:  # case_type == CaseType.HF_ONLY:
            raise RuntimeError("Cannot predict LF when only trained using HF samples.")

    def hf_predict(pred_x):
        if case_type == CaseType.HF_ONLY:
            return sf_ridge_gp_predict(
                pred_x,
                hf_training_data,
                training_artifacts,
            )
        elif case_type == CaseType.MULTI_FIDELITY:
            _, _, hf_means, hf_variances = mf_deep_ridge_gp_predict(
                pred_x,
                lf_training_data,
                hf_training_data,
                training_artifacts,
                num_lf_predictions=num_lf_predictions,
                lf_predictions_seed=lf_predictions_seed,
            )
            return hf_means, hf_variances
        else:  # case_type == CaseType.LF_ONLY:
            raise RuntimeError("Cannot predict HF when only trained using LF samples.")

    def validation_routine(means, variances, actual):
        return compute_validation_metrics(
            means,
            variances,
            actual[:, None],  # need a column vector
            confidence_interval_bounds_cdf_values=ci_bounds,
            validation_predictions_random_seed=val_pred_rand_seed,
            num_gp_samples=num_gp_samples,
        )

    # Validation Artifacts
    validation_artifacts = {}

    # LF training and LF validation
    # LF predicted vs. HF actual at HF validation points
    # (how well does the LF predictive model perform on its own?)
    if case_type in [CaseType.LF_ONLY, CaseType.MULTI_FIDELITY]:
        validation_artifacts.update(
            {
                "lf_training": validation_routine(
                    *lf_predict(lf_training_data["x"]), lf_training_data["y"]
                ),
                "lf_validation": validation_routine(
                    *lf_predict(lf_validation_data["x"]), lf_validation_data["y"]
                ),
                "hf_actual_vs_lf_predicted": validation_routine(
                    *lf_predict(hf_validation_data["x"]), hf_validation_data["y"]
                ),
            }
        )

    # HF training and validation
    if case_type in [CaseType.HF_ONLY, CaseType.MULTI_FIDELITY]:
        validation_artifacts.update(
            {
                "hf_training": validation_routine(
                    *hf_predict(hf_training_data["x"]), hf_training_data["y"]
                ),
                "hf_validation": validation_routine(
                    *hf_predict(hf_validation_data["x"]), hf_validation_data["y"]
                ),
            }
        )

        # # Save preparation artifacts in the temp file
        # with open(temp_file_path, "wb") as temp_file:
        #     dill.dump(validation_artifacts, temp_file)

    return validation_artifacts


def save_results(
    case_number,
    worker_uid,
    case_parameters,
    process_parameters,
    preparation_artifacts,
    training_artifacts,
    validation_artifacts,
):
    """Persist all case inputs, along with training and validation artifacts."""

    # Results file path
    results_file_path = RESULTS_DIR / "{}.h5".format(case_number)

    # Append timestamp if an artifact with the same name already exists
    if results_file_path.is_file():
        results_file_path = RESULTS_DIR / "{}_{}.h5".format(
            case_number, compact_timestamp()
        )

    # We only save the indices in the results, not the actual data
    preparation_artifacts_filtered = {
        key: preparation_artifacts[key]
        for key in preparation_artifacts.keys()
        if key
        not in [
            "lf_training_data",
            "lf_validation_data",
            "hf_training_data",
            "hf_validation_data",
        ]
    }

    # Case artifacts
    case_artifacts = {
        "doe_name": DOE_NAME,
        "case_number": case_number,
        "worker_uid": worker_uid,
        "case_parameters": case_parameters,
        "process_parameters": process_parameters,
        "preparation_artifacts": preparation_artifacts_filtered,
        "training_artifacts": training_artifacts,
        "validation_artifacts": validation_artifacts,
    }

    # Write results file
    with h5.File(results_file_path, "x") as results_file:
        pydict_to_h5group(case_artifacts, results_file)


def clean_up_temp_dir(run_temp_dir):
    # Delete the temporary run directory
    shutil.rmtree(run_temp_dir)


###################
# Dataset Loading #
###################


def load_dataset(dataset_name, output_name):
    with h5.File(DATASETS_DIR / "{}.h5".format(dataset_name), "r") as dataset:
        x = np.array(dataset["inputs"])
        y = np.array(dataset["outputs"][output_name])

        # y should be a scalar
        if len(y.shape) > 2 or (len(y.shape) == 2 and y.shape[1] > 1):
            raise RuntimeError("The output should be scalar.")

        # y should be a flat vector
        y = y.flatten()

        dy_dx = (
            np.array(dataset["gradients"][output_name])
            if "gradients" in dataset
            else None
        )
    return x, y, dy_dx


########################################
# Reproducible Dataset Split Generator #
########################################


def generate_reproducible_dataset_split(
    dataset_split_random_seed, num_training_samples, num_total_samples
):
    np.random.seed(dataset_split_random_seed)  # fix random seed for reproducibility
    all_indices = np.arange(num_total_samples)
    training_indices = np.sort(
        np.random.choice(all_indices, num_training_samples, replace=False)
    )
    validation_indices = np.setdiff1d(all_indices, training_indices)
    return training_indices, validation_indices


def prepare_training_validation_indices(
    dataset_name, output_name, num_training_samples, dataset_split_random_seed
):
    x, _, _ = load_dataset(dataset_name, output_name)
    training_indices, validation_indices = generate_reproducible_dataset_split(
        dataset_split_random_seed,
        num_training_samples,
        x.shape[0],
    )
    return training_indices, validation_indices


def get_training_validation_sets(
    dataset_name, output_name, training_indices, validation_indices
):
    x, y, _ = load_dataset(dataset_name, output_name)
    training_data = {"x": x[training_indices], "y": y[training_indices]}
    validation_data = {"x": x[validation_indices], "y": y[validation_indices]}
    return training_data, validation_data


######################
# Validation Metrics #
######################


def compute_validation_metrics(
    predicted_means,
    predicted_variances,
    actual_y,
    confidence_interval_bounds_cdf_values=[0.025, 0.975],
    validation_predictions_random_seed=0,
    num_gp_samples=10,
):
    # Quantile values need to be arrays
    confidence_interval_bounds_cdf_values = jnp.array(
        confidence_interval_bounds_cdf_values
    )

    # We start with point-based predictions and confidence interval bounds
    # Depending on whether the method is fully Bayesian or not, the computations can be
    # made analytically or require sampling the posterior predictive distribution
    if predicted_means.shape[1] == 1:
        point_based_predictions = predicted_means
        confidence_interval_bounds = dist.Normal(
            predicted_means, jnp.sqrt(predicted_variances)
        ).icdf(confidence_interval_bounds_cdf_values)
    else:
        prng_key = PRNGKey(validation_predictions_random_seed)
        num_samples = num_gp_samples
        num_predictions = predicted_means.shape[0]
        num_posterior_draws = predicted_means.shape[1]
        samples = (
            dist.Normal(loc=predicted_means, scale=jnp.sqrt(predicted_variances))
            .sample(prng_key, sample_shape=(num_samples,))
            .transpose((1, 2, 0))
            .reshape((num_predictions, num_posterior_draws * num_samples))
        )

        quantiles = jnp.quantile(
            samples,
            jnp.concatenate((jnp.array([0.5]), confidence_interval_bounds_cdf_values)),
            axis=1,
        ).T

        point_based_predictions = quantiles[:, 0, None]
        confidence_interval_bounds = quantiles[:, 1:]

    # Global error metrics
    (
        rmse,
        nrmse,
        r_squared,
        absolute_pointwise_error,
    ) = rmse_nrmse_r_squared_and_absolute_error(point_based_predictions, actual_y)

    mlppd, lppd = mean_log_pointwise_predictive_density_and_lppd(
        predicted_means, predicted_variances, actual_y
    )

    return {
        "rmse": rmse,
        "nrmse": nrmse,
        "r_squared": r_squared,
        "mlppd": mlppd,
        "point_based_predictions": point_based_predictions,
        "confidence_interval_bounds": confidence_interval_bounds,
        "absolute_pointwise_error": absolute_pointwise_error,
        "lppd": lppd,
    }


#################
# Error Metrics #
#################


@jit
def rmse_nrmse_r_squared_and_absolute_error(y_predicted, y_actual):
    var_y_actual = jnp.var(y_actual)
    absolute_error = (y_predicted - y_actual).flatten()
    mse = jnp.mean(jnp.square(absolute_error))
    r_squared = 1 - mse / var_y_actual
    rmse = jnp.sqrt(mse)
    nrmse = rmse / jnp.sqrt(var_y_actual)
    return rmse, nrmse, r_squared, absolute_error


@jit
def mean_log_pointwise_predictive_density_and_lppd(y_means, y_variances, y_actual):
    lppd = (
        logsumexp(
            dist.Normal(loc=y_means, scale=jnp.sqrt(y_variances)).log_prob(y_actual),
            axis=1,
        )
        - jnp.log(y_means.shape[1])
    )
    mlppd = jnp.mean(lppd, axis=0)
    return mlppd, lppd


#######################
# Persistence Helpers #
#######################


def compact_timestamp():
    return "{:%Y%m%d_%H%M%S}".format(datetime.datetime.now())


def pydict_to_h5group(pydict: dict, h5group: h5.Group):
    for key, value in pydict.items():
        if isinstance(value, dict):
            subgroup = h5group.create_group(str(key))
            pydict_to_h5group(value, subgroup)
        else:
            # Conversions for certain types
            if isinstance(value, jax.interpreters.xla.DeviceArray):
                value = np.array(value)
            elif isinstance(value, enum.Enum):
                value = value.value

            # Save
            if isinstance(value, np.ndarray):
                if value.size == 1:
                    h5group.attrs[key] = value.flatten()[0]
                else:
                    h5group.create_dataset(key, data=value)
            else:
                h5group.attrs[key] = value


###############################
# File-based DOE Case Manager #
###############################


class CaseState(str, enum.Enum):
    UNSTARTED = "unstarted"
    RUNNING = "running"
    FINISHED = "finished"
    INTERRUPTED = "interrupted"
    FAILED = "failed"


def _get_cases_by_state(case_state: CaseState) -> List[str]:
    path = STATUS_DIR / case_state.value
    return [file.name for file in path.glob("*")]


def _change_state(
    case_number: str, current_state: CaseState, new_state: CaseState
) -> None:
    old_path = STATUS_DIR / current_state.value / case_number
    new_path = STATUS_DIR / new_state.value / case_number
    if old_path.is_file():
        shutil.move(old_path, new_path)
    else:
        raise RuntimeError(
            "Trying to change state of case {} from {} to {} failed.".format(
                case_number, current_state, new_state
            )
        )


def _get_random_case_to_run_from_list(worker_uid: str, case_state: CaseState) -> int:
    cases = _get_cases_by_state(case_state)
    if cases:
        case_number = random.choice(cases)
        _change_state(case_number, case_state, CaseState.RUNNING)
        with open(STATUS_DIR / CaseState.RUNNING.value / case_number, "a") as file:
            file.write("{}\n".format(worker_uid))
        return case_number
    return None


def get_case_to_run(worker_uid, testing=False) -> int:
    # This is a hack to reduce the likelihood of race conditions between workers
    # We wait a random amount of time between 0s and 10s before picking a new case
    if not testing:
        time.sleep(random.uniform(0, 10))

    # If there are interrupted cases, we send these in priority
    case = _get_random_case_to_run_from_list(worker_uid, CaseState.INTERRUPTED)
    if case:
        log("({}) Case {} resumed.".format(worker_uid, case))
        return case

    # Otherwise we send an unstarted case
    case = _get_random_case_to_run_from_list(worker_uid, CaseState.UNSTARTED)
    if case:
        log("({}) Case {} started.".format(worker_uid, case))
        return case

    # If there were no unstarted cases either, then None is returned
    log("({}) No case to run.".format(worker_uid))
    return None


def report_interrupted(case_number: int, worker_uid: str):
    _change_state(case_number, CaseState.RUNNING, CaseState.INTERRUPTED)
    log("({}) Case {} interrupted.".format(worker_uid, case_number))


def report_finished(case_number: int, worker_uid: str):
    _change_state(case_number, CaseState.RUNNING, CaseState.FINISHED)
    log("({}) Case {} finished.".format(worker_uid, case_number))


def report_failed(case_number: int, worker_uid: str):
    _change_state(case_number, CaseState.RUNNING, CaseState.FAILED)
    log("({}) Case {} failed.".format(worker_uid, case_number))


def clean_up_running_dir(worker_uid):
    # Logging
    log("({}) Started cleaning up the `running` directory.".format(worker_uid))

    # List all running workers
    try:
        # `grep` returns a status code of 1 if no lines are returned
        running_workers = [
            line.split(".")[0]
            for line in subprocess.check_output(
                "qstat -u rgautier6 -t | grep ' R '", shell=True, text=True
            ).split("\n")
        ]
    except subprocess.CalledProcessError:
        # if no workers are running
        running_workers = []

    # Iterate through each file in the `running` directory and change the case's state
    # to `interrupted` if its file does not contain the ID of a currently running worker
    for file_path in (STATUS_DIR / CaseState.RUNNING.value).glob("*"):
        if file_path.is_file():  # in case the file has been moved
            with open(file_path, "r") as file:
                last_subworker_uid = file.readlines()[-1]
            last_worker_uid = last_subworker_uid[: last_subworker_uid.rfind("[")]
            if last_worker_uid not in running_workers:
                try:
                    _change_state(
                        file_path.name, CaseState.RUNNING, CaseState.INTERRUPTED
                    )
                except Exception:
                    # it's okay if it fails, another worker probably cleaned up
                    # that file before
                    pass

    # Logging
    log("({}) Finished cleaning up the `running` directory.".format(worker_uid))


#####################
# DOE Reading Utils #
#####################

_process_parameters = None
_cases = None


def get_process_parameters() -> Dict:
    global _process_parameters
    if _process_parameters is None:
        with open(PROCESS_PARAMETERS_FILE) as file:
            _process_parameters = json.load(file)
    return _process_parameters


def get_all_cases() -> pd.DataFrame:
    global _cases
    if _cases is None:
        _cases = pd.read_csv(CASES_FILE, header=0, index_col=0)
    return _cases


def get_case_parameters(case_number: int) -> Dict:
    return dict(get_all_cases().loc[case_number])


###################
# Logging Utility #
###################


def log(message):
    LOG_FILE.touch()
    with open(LOG_FILE, "a") as file:
        file.write("{}: {}\n".format(compact_timestamp(), message))


##################
# DOE Generation #
##################


def generate_doe_cases(
    dataset_pairs,
    alt_allocation_ratios,
    alt_dataset_split_random_seeds,
    alt_feature_space_relation,
    alt_dim_fs,
):
    # Initialize list of cases
    cases = []

    # Iterate through each dataset pair
    for pair_name, pair_info in dataset_pairs.items():

        # Retrieve LF and HF dataset names
        lf_dataset_name = pair_info["lf"]
        hf_dataset_name = pair_info["hf"]

        # Retrieve needed dataset metadata
        # 💡 We assume LF and HF datasets have the same input domain
        with h5.File(DATASETS_DIR / "{}.h5".format(lf_dataset_name), "r") as lf_dataset:
            lf_cost = np.mean(lf_dataset["durations"])

        with h5.File(DATASETS_DIR / "{}.h5".format(hf_dataset_name), "r") as hf_dataset:
            hf_cost = np.mean(hf_dataset["durations"])

        # Generate all cases for this pair of datasets
        for (
            output_name,
            budget_in_hf_samples,
            allocation_ratio,
            dataset_split_random_seed,
            feature_space_relation,
            dim_fs,
        ) in it.product(
            pair_info["outputs"],
            pair_info["total_cost_sweep_in_hf_samples"],
            alt_allocation_ratios,
            alt_dataset_split_random_seeds,
            alt_feature_space_relation,
            alt_dim_fs,
        ):
            # Renove duplicate cases if the case is really single fidelity
            if allocation_ratio not in [0.0, 1.0] or feature_space_relation == "shared":
                # Computing the respective numbers of LF and HF training samples based
                # on the desired allocation ratio and total budget
                num_hf_training_samples = int(
                    (1 - allocation_ratio) * budget_in_hf_samples
                )
                num_lf_training_samples = int(
                    allocation_ratio * hf_cost / lf_cost * budget_in_hf_samples
                )

                cases.append(
                    {
                        "dataset_pair": pair_name,
                        "lf_dataset_name": lf_dataset_name,
                        "hf_dataset_name": hf_dataset_name,
                        "output_name": output_name,
                        "budget_in_hf_samples": budget_in_hf_samples,
                        "cost_ratio": hf_cost / lf_cost,
                        "allocation_ratio": allocation_ratio,
                        "num_lf_training_samples": num_lf_training_samples,
                        "num_hf_training_samples": num_hf_training_samples,
                        "lf_dataset_split_random_seed": dataset_split_random_seed[0],
                        "hf_dataset_split_random_seed": dataset_split_random_seed[1],
                        "feature_space_relation": feature_space_relation,
                        "dim_feature_space": dim_fs,
                    }
                )
    return cases


def generate_doe_cases_custom_dims(
    dataset_pairs,
    alt_allocation_ratios,
    alt_dataset_split_random_seeds,
    alt_feature_space_relation,
):
    # Initialize list of cases
    cases = []

    # Iterate through each dataset pair
    for pair_name, pair_info in dataset_pairs.items():

        # Retrieve LF and HF dataset names
        lf_dataset_name = pair_info["lf"]
        hf_dataset_name = pair_info["hf"]

        # Retrieve needed dataset metadata
        # 💡 We assume LF and HF datasets have the same input domain
        with h5.File(DATASETS_DIR / "{}.h5".format(lf_dataset_name), "r") as lf_dataset:
            lf_cost = np.mean(lf_dataset["durations"])

        with h5.File(DATASETS_DIR / "{}.h5".format(hf_dataset_name), "r") as hf_dataset:
            hf_cost = np.mean(hf_dataset["durations"])

        # Generate all cases for this pair of datasets
        for (
            output_name,
            budget_in_hf_samples,
            dim_fs,
            allocation_ratio,
            dataset_split_random_seed,
            feature_space_relation,
        ) in it.product(
            pair_info["outputs"],
            pair_info["total_cost_sweep_in_hf_samples"],
            pair_info["dim_fs"],
            alt_allocation_ratios,
            alt_dataset_split_random_seeds,
            alt_feature_space_relation,
        ):
            # Remove duplicate cases if the case is really single fidelity
            if allocation_ratio not in [0.0, 1.0] or feature_space_relation == "shared":
                # Computing the respective numbers of LF and HF training samples based
                # on the desired allocation ratio and total budget
                num_hf_training_samples = int(
                    (1 - allocation_ratio) * budget_in_hf_samples
                )
                num_lf_training_samples = int(
                    allocation_ratio * hf_cost / lf_cost * budget_in_hf_samples
                )

                cases.append(
                    {
                        "dataset_pair": pair_name,
                        "lf_dataset_name": lf_dataset_name,
                        "hf_dataset_name": hf_dataset_name,
                        "output_name": output_name,
                        "budget_in_hf_samples": budget_in_hf_samples,
                        "cost_ratio": hf_cost / lf_cost,
                        "allocation_ratio": allocation_ratio,
                        "num_lf_training_samples": num_lf_training_samples,
                        "num_hf_training_samples": num_hf_training_samples,
                        "lf_dataset_split_random_seed": dataset_split_random_seed[0],
                        "hf_dataset_split_random_seed": dataset_split_random_seed[1],
                        "feature_space_relation": feature_space_relation,
                        "dim_feature_space": dim_fs,
                    }
                )
    return cases
