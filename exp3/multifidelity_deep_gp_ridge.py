from functools import partial

from jax import numpy as jnp, jit
from jax.lax import map as jmap
from jax.random import PRNGKey
from numpyro import enable_x64, sample, deterministic, distributions as dist

from .multifidelity_deep_gp import (
    mf_deep_gp_model,
    dmf_gp_lf_predict_loop_1,
    dmf_gp_hf_predict_loop_1,
)
from .models_utils import (
    denormalize,
    householder_reparameterization,
    mcmc,
    normalize,
    normalize_mf_training_data,
    subspace_angles,
)

enable_x64()

############################################################
# Probabilistic Model for the Ridge Version of the Deep GP #
############################################################


def mf_deep_ridge_gp_model(
    lf_training_data,
    hf_training_data,
    feature_space_relation,
    dim_feature_space,
    jitter=1e-6,
    use_alternate_priors=False,
):
    # Problem dimensions
    m = dim_feature_space
    n = lf_training_data["x"].shape[1]
    num_projection_parameters = m * n - m * (m - 1) // 2

    if feature_space_relation == "shared":
        # If feature spaces are shared, we only need one set of projection parameters

        # Priors
        projection_parameters = sample(
            "projection_parameters",
            dist.Normal(
                jnp.zeros((num_projection_parameters,)),
                jnp.ones((num_projection_parameters,)),
            ),
        )

        # Compute projection matrix
        w_lf = householder_reparameterization(projection_parameters, n, m)
        w_hf = w_lf

    else:
        # If feature spaces are different or related, we need two sets of projection
        # parameters

        # Priors
        lf_projection_parameters = sample(
            "lf_projection_parameters",
            dist.Normal(
                jnp.zeros((num_projection_parameters,)),
                jnp.ones((num_projection_parameters,)),
            ),
        )

        # Priors
        hf_projection_parameters = sample(
            "hf_projection_parameters",
            dist.Normal(
                jnp.zeros((num_projection_parameters,)),
                jnp.ones((num_projection_parameters,)),
            ),
        )

        # Compute different projection matrices for LF and HF
        w_lf = householder_reparameterization(lf_projection_parameters, n, m)
        w_hf = householder_reparameterization(hf_projection_parameters, n, m)

        if feature_space_relation == "related":
            # If the feature spaces are related, there is a prior joint distribution of
            # the two projection matrices with their subspace angle
            angles = deterministic("subspace_angles", subspace_angles(w_lf, w_hf))
            sample(
                "subspace_angles_obs",
                dist.HalfNormal(
                    0.4 * jnp.ones((dim_feature_space,)),
                ),
                obs=angles,
            )

    # Project inputs
    proj_lf_training_data = {
        "x": lf_training_data["x"] @ w_lf,
        "y": lf_training_data["y"],
    }
    proj_hf_training_data = {
        "x": hf_training_data["x"] @ w_hf,
        "y": hf_training_data["y"],
    }

    return mf_deep_gp_model(
        proj_lf_training_data,
        proj_hf_training_data,
        jitter=jitter,
        use_alternate_priors=use_alternate_priors,
    )


##############################################################
# Prediction Functions for the Low- and High-Fidelity Models #
##############################################################

# Body of the loop over the posterior draws (`posterior_draw` is one element)
def dmf_ridge_gp_lf_predict_loop_1(
    norm_pred_x,
    lf_training_data,
    dim_feature_space,
    feature_space_relation,
    posterior_draw,
    jitter=1e-8,
):
    # Problem dimensions
    dim_inputs = norm_pred_x.shape[1]

    # Retrieve projection matrix
    projection_parameters = (
        posterior_draw["projection_parameters"]
        if feature_space_relation == "shared"
        else posterior_draw["lf_projection_parameters"]
    )
    w_lf = householder_reparameterization(
        projection_parameters, dim_inputs, dim_feature_space
    )

    # Project prediction sites
    proj_pred_x = norm_pred_x @ w_lf

    # Project LF training data
    proj_lf_training_data = {
        "x": lf_training_data["x"] @ w_lf,
        "y": lf_training_data["y"],
    }

    return dmf_gp_lf_predict_loop_1(
        proj_pred_x, posterior_draw, proj_lf_training_data, jitter=jitter
    )


@partial(jit, static_argnums=(3, 4))
def lf_predict(
    norm_pred_x,
    posterior_draws,
    norm_lf_training_data,
    dim_feature_space,
    feature_space_relation,
    jitter=1e-8,
):
    # The most outer loop in the prediction routine uses jmap instead of vmap to reduce
    # the memory footprint
    norm_lf_means, norm_lf_variances = jmap(
        partial(
            dmf_ridge_gp_lf_predict_loop_1,
            norm_pred_x,
            norm_lf_training_data,
            dim_feature_space,
            feature_space_relation,
            jitter=jitter,
        ),
        posterior_draws,
    )
    return norm_lf_means.T, norm_lf_variances.T


# Body of the loop over the posterior draws (`posterior_draw` is one element)
# Note: `norm_lf_y_samples` is now size (num_predictions x num_lf_samples)
def dmf_ridge_gp_hf_predict_loop_1(
    norm_pred_x,
    norm_hf_training_data,
    dim_feature_space,
    feature_space_relation,
    norm_lf_y_samples_and_posterior_draw,
    jitter=1e-8,
):
    # Extract `norm_lf_y_samples` and `posterior_draw`
    norm_lf_y_samples = norm_lf_y_samples_and_posterior_draw[0]
    posterior_draw = norm_lf_y_samples_and_posterior_draw[1]

    # Problem dimensions
    dim_inputs = norm_pred_x.shape[1]

    # Retrieve projection matrix
    projection_parameters = (
        posterior_draw["projection_parameters"]
        if feature_space_relation == "shared"
        else posterior_draw["hf_projection_parameters"]
    )
    w_hf = householder_reparameterization(
        projection_parameters, dim_inputs, dim_feature_space
    )

    # Project prediction sites
    proj_pred_x = norm_pred_x @ w_hf

    # Project LF training data
    proj_hf_training_data = {
        "x": norm_hf_training_data["x"] @ w_hf,
        "y": norm_hf_training_data["y"],
    }

    return dmf_gp_hf_predict_loop_1(
        proj_pred_x,
        norm_lf_y_samples,
        posterior_draw,
        proj_hf_training_data,
        jitter=jitter,
    )


@partial(jit, static_argnums=(4, 5))
def hf_predict(
    norm_pred_x,
    posterior_draws,
    norm_hf_training_data,
    norm_lf_y_samples,
    dim_feature_space,
    feature_space_relation,
    jitter=1e-8,
):
    # The most outer loop in the prediction routine uses jmap instead of vmap to reduce
    # the memory footprint
    norm_hf_means, norm_hf_variances = jmap(
        partial(
            dmf_ridge_gp_hf_predict_loop_1,
            norm_pred_x,
            norm_hf_training_data,
            dim_feature_space,
            feature_space_relation,
            jitter=jitter,
        ),
        (norm_lf_y_samples, posterior_draws),
    )
    return jnp.transpose(norm_hf_means, (1, 0, 2)), jnp.transpose(
        norm_hf_variances, (1, 0, 2)
    )
    # originally num_posterior_draws x num_predictions x num_lf_predictions
    # transposed to num_predictions x num_posterior_draws x num_lf_predictions


####################
# Training Routine #
####################


def mf_deep_ridge_gp_train(
    lf_training_data,
    hf_training_data,
    mcmc_parameters,
    feature_space_relation,
    dim_feature_space,
    mcmc_file_path=None,
    use_alternate_priors=False,
):
    # Normalize training data
    (
        norm_lf_training_data,
        norm_hf_training_data,
        normalization_constants,
    ) = normalize_mf_training_data(lf_training_data, hf_training_data)

    # Sample from the posterior chains using MCMC
    posterior_draws, training_duration = mcmc(
        # model,
        mf_deep_ridge_gp_model,
        (
            norm_lf_training_data,
            norm_hf_training_data,
            feature_space_relation,
            dim_feature_space,
        ),
        {"use_alternate_priors": use_alternate_priors},
        mcmc_file_path=mcmc_file_path,
        **mcmc_parameters,
    )

    return {
        "dim_feature_space": dim_feature_space,
        "feature_space_relation": feature_space_relation,
        "normalization_constants": normalization_constants,
        "posterior_draws": posterior_draws,
        "training_duration": training_duration,
    }


######################
# Prediction Routine #
######################


def mf_deep_ridge_gp_predict(
    pred_x,
    lf_training_data,
    hf_training_data,
    training_artifacts,
    num_lf_predictions=10,
    lf_predictions_seed=675,
):
    # Retrieve quantities of interest
    posterior_draws = training_artifacts["posterior_draws"]
    normalization_constants = training_artifacts["normalization_constants"]
    dim_feature_space = training_artifacts["dim_feature_space"]
    feature_space_relation = training_artifacts["feature_space_relation"]

    # Normalize the training data and the prediction sites
    norm_lf_training_data, norm_hf_training_data = normalize_mf_training_data(
        lf_training_data, hf_training_data, normalization_constants
    )
    norm_pred_x = normalize(
        pred_x,
        normalization_constants["x_offset"],
        normalization_constants["x_scaling"],
    )

    # Problem dimensions
    num_predictions = pred_x.shape[0]
    num_posterior_draws = posterior_draws[list(posterior_draws.keys())[0]].shape[0]

    # LF GP
    norm_lf_means, norm_lf_variances = lf_predict(
        norm_pred_x,
        posterior_draws,
        norm_lf_training_data,
        dim_feature_space,
        feature_space_relation,
    )

    norm_lf_samples = (
        dist.Normal(loc=norm_lf_means, scale=jnp.sqrt(norm_lf_variances))
        .sample(PRNGKey(lf_predictions_seed), sample_shape=(num_lf_predictions,))
        .transpose((2, 1, 0))
        # originally num_lf_predictions x num_predictions x num_posterior_draws
        # transposed to num_posterior_draws x num_predictions x num_lf_predictions
    )

    # Second GP
    norm_hf_means, norm_hf_variances = hf_predict(
        norm_pred_x,
        posterior_draws,
        norm_hf_training_data,
        norm_lf_samples,
        dim_feature_space,
        feature_space_relation,
    )

    # Denormalization and flattening of all stochastic dimensions ##################

    lf_means = denormalize(
        norm_lf_means,
        normalization_constants["y_offset"],
        normalization_constants["y_scaling"],
    ).reshape((num_predictions, num_posterior_draws))

    lf_variances = (
        norm_lf_variances * normalization_constants["y_scaling"] ** 2
    ).reshape((num_predictions, num_posterior_draws))

    hf_means = denormalize(
        norm_hf_means,
        normalization_constants["y_offset"],
        normalization_constants["y_scaling"],
    ).reshape((num_predictions, num_posterior_draws * num_lf_predictions))

    hf_variances = (
        norm_hf_variances * normalization_constants["y_scaling"] ** 2
    ).reshape((num_predictions, num_posterior_draws * num_lf_predictions))

    return lf_means, lf_variances, hf_means, hf_variances
