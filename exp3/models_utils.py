from enum import Enum
import time

import dill
from jax import numpy as jnp
from jax.ops import index, index_update
from jax.random import PRNGKey
from jax.scipy.linalg import svd
import numpy as np
from numpyro.diagnostics import print_summary
from numpyro.infer.util import initialize_model
from numpyro.infer.hmc import hmc
from numpyro.util import fori_collect
from scipy.stats.distributions import norm


######
# GP #
######


def squared_distance(x1, x2):
    return (
        jnp.sum(x1 ** 2, axis=1, keepdims=True)
        - 2.0 * x1 @ x2.T
        + jnp.sum(x2 ** 2, axis=1, keepdims=True).T
    )


def se_kernel(x1, x2, signal_variance, length_scales):
    return signal_variance * jnp.exp(
        -0.5 * squared_distance(x1 / length_scales, x2 / length_scales)
    )


#################################
# Householder Reparametrization #
#################################


def householder(v, n):
    v = v[:, None]
    k = v.shape[0]
    sgn = jnp.sign(v[0])
    u = v + sgn * jnp.linalg.norm(v) * jnp.eye(k, 1)
    u = u / jnp.linalg.norm(u)
    h_k = -sgn * (jnp.eye(k) - 2 * u @ u.T)
    h_k_hat = jnp.eye(n)
    start = n - k
    h_k_hat = index_update(h_k_hat, index[start:, start:], h_k)
    return h_k_hat


def householder_reparameterization(
    projection_parameters, num_original_inputs, num_active_dims
):
    m, n = num_active_dims, num_original_inputs
    h = jnp.eye(n)
    i_end = 0
    for i in range(m):
        i_start = i_end
        i_end = i_start + n - i
        h_k_hat = householder(projection_parameters[i_start:i_end], n)
        h = h_k_hat @ h
    return h[:, :m]


#################
# Normalization #
#################


def compute_normalization(x):
    """Normalize vector `x` using its mean and standard deviation."""
    return np.mean(x), np.std(x)


def normalize(x, offset, scaling):
    return (x - offset) / scaling


def denormalize(x, offset, scaling):
    return offset + x * scaling


def apply_normalization(training_data, nc):
    return {
        "x": normalize(training_data["x"], nc["x_offset"], nc["x_scaling"]),
        "y": normalize(training_data["y"], nc["y_offset"], nc["y_scaling"]),
    }


def normalize_training_data(training_data, normalization_constants=None):
    """Normalize a dict of the sort {'x': ..., 'y': ...}"""
    if normalization_constants is None:
        # Normalize inputs
        x_offset, x_scaling = compute_normalization(training_data["x"])

        # Normalize outputs
        y_offset, y_scaling = compute_normalization(training_data["y"])

        # Package normalization constants
        normalization_constants = {
            "x_offset": x_offset,
            "x_scaling": x_scaling,
            "y_offset": y_offset,
            "y_scaling": y_scaling,
        }

        return (
            apply_normalization(training_data, normalization_constants),
            normalization_constants,
        )
    else:
        return apply_normalization(training_data, normalization_constants)


def normalize_mf_training_data(
    lf_training_data, hf_training_data, normalization_constants=None
):
    if normalization_constants is None:
        num_lf_training_samples = lf_training_data["x"].shape[0]
        training_data = {
            "x": np.concatenate((lf_training_data["x"], hf_training_data["x"]), 0),
            "y": np.concatenate((lf_training_data["y"], hf_training_data["y"]), 0),
        }
        norm_training_data, normalization_constants = normalize_training_data(
            training_data
        )
        norm_lf_training_data = {
            "x": norm_training_data["x"][:num_lf_training_samples, :],
            "y": norm_training_data["y"][:num_lf_training_samples],
        }
        norm_hf_training_data = {
            "x": norm_training_data["x"][num_lf_training_samples:, :],
            "y": norm_training_data["y"][num_lf_training_samples:],
        }
        return norm_lf_training_data, norm_hf_training_data, normalization_constants
    else:
        return normalize_training_data(
            lf_training_data, normalization_constants
        ), normalize_training_data(hf_training_data, normalization_constants)


########
# MCMC #
########


class MCMCState(Enum):
    WARMUP = 1
    SAMPLING = 2


def mcmc(
    numpyro_model,
    model_args,
    model_kwargs,
    mcmc_file_path=None,
    target_acceptance_probability=0.8,
    num_warmup_draws=500,
    num_posterior_draws=1000,
    num_draws_between_saves=10,
    random_seed=0,
    progress_bar=True,
    display_summary=True,
):
    # Does `mcmc_file_path` exist?
    if mcmc_file_path is not None and mcmc_file_path.is_file():
        # If it does, we use it to retrieve the latest MCMC state
        with open(mcmc_file_path, "rb") as mcmc_file:
            saved_state = dill.load(mcmc_file)
        model_info = saved_state["model_info"]
        hmc_state = saved_state["hmc_state"]
        remaining_warmup_draws = saved_state["remaining_warmup_draws"]
        remaining_sampling_draws = saved_state["remaining_sampling_draws"]
        training_duration = saved_state["training_duration"]
        posterior_draws = saved_state["posterior_draws"]

        init_kernel, sample_kernel = hmc(model_info.potential_fn, algo="NUTS")
        _ = init_kernel(
            model_info.param_info,
            num_warmup=remaining_warmup_draws,
            target_accept_prob=target_acceptance_probability,
        )

    else:
        # Otherwise, we initialize a new state

        # Manual MCMC initialization
        model_info = initialize_model(
            PRNGKey(random_seed),
            numpyro_model,
            model_args=model_args,
            model_kwargs=model_kwargs,
        )
        init_kernel, sample_kernel = hmc(model_info.potential_fn, algo="NUTS")
        hmc_state = init_kernel(
            model_info.param_info,
            num_warmup=num_warmup_draws,
            target_accept_prob=target_acceptance_probability,
        )

        # Counters and draws initializations
        remaining_warmup_draws = num_warmup_draws
        remaining_sampling_draws = num_posterior_draws
        training_duration = 0
        posterior_draws = None

    # We keep sampling until we are done
    while remaining_sampling_draws > 0:

        if remaining_warmup_draws > 0:
            # Warmup
            state = MCMCState.WARMUP
            upper = min(remaining_warmup_draws, num_draws_between_saves)
        else:
            state = MCMCState.SAMPLING
            upper = min(remaining_sampling_draws, num_draws_between_saves)

        start_time = time.time()

        new_draws, hmc_state = fori_collect(
            0,
            upper,
            sample_kernel,
            hmc_state,
            progbar=progress_bar,
            return_last_val=True,
            transform=lambda state: model_info.postprocess_fn(state.z),
        )

        training_duration += time.time() - start_time

        # Subtract number of draws, update posterior draws if applicable
        if state == MCMCState.WARMUP:
            remaining_warmup_draws -= upper
        elif state == MCMCState.SAMPLING:
            remaining_sampling_draws -= upper
            posterior_draws = (
                new_draws
                if posterior_draws is None
                else {
                    key: jnp.concatenate((posterior_draws[key], new_draws[key]))
                    for key in new_draws.keys()
                }
            )

        # Save progress
        with open(mcmc_file_path, "wb") as mcmc_file:
            dill.dump(
                {
                    "model_info": model_info,
                    "hmc_state": hmc_state,
                    "training_duration": training_duration,
                    "remaining_warmup_draws": remaining_warmup_draws,
                    "remaining_sampling_draws": remaining_sampling_draws,
                    "posterior_draws": posterior_draws,
                },
                mcmc_file,
            )

    if display_summary:
        print_summary(
            {key: value[None, ...] for key, value in posterior_draws.items()}, prob=0.9
        )

    return posterior_draws, training_duration


##################################
# Post-Processing of MCMC chains #
##################################


def process_chains(
    posterior_draws, grouped_by_chain, ungroup=False, num_thinned_draws=None
):
    # Alternate dictionary holding the newly processed chains
    processed_draws = {}

    for site_name, site_draws in posterior_draws.items():
        # Whether the current site draws is grouped by chain
        site_draws_grouped_by_chain = grouped_by_chain

        # Flatten if necessary
        if site_draws_grouped_by_chain and ungroup:
            processed_draws[site_name] = jnp.reshape(
                site_draws, (-1,) + site_draws.shape[2:]
            )
            site_draws_grouped_by_chain = False
        else:
            processed_draws[site_name] = site_draws

        # Thin if necessary
        if num_thinned_draws is not None:
            chain_length = (
                processed_draws[site_name].shape[1]
                if site_draws_grouped_by_chain
                else processed_draws[site_name].shape[0]
            )
            if num_thinned_draws < chain_length:
                thinning_factor = chain_length // num_thinned_draws
                last_index = thinning_factor * num_thinned_draws

                if site_draws_grouped_by_chain:
                    processed_draws[site_name] = processed_draws[site_name][
                        :, :last_index:thinning_factor
                    ]
                else:
                    processed_draws[site_name] = processed_draws[site_name][
                        :last_index:thinning_factor
                    ]

    return processed_draws


##########################################################
# Find an Orthogonal Basis for the Orthogonal Complement #
##########################################################


def construct_w_orth(w):
    # Problem parameters
    dim_inputs = w.shape[0]
    dim_fs = w.shape[1]
    dim_orth_fs = dim_inputs - dim_fs

    mat = np.concatenate((w, norm().rvs(size=(dim_inputs, dim_orth_fs))), axis=1)
    for i in range(dim_fs, dim_inputs):
        mat[:, i] -= np.sum(mat[:, :i] * (mat[:, i, None].T @ mat[:, :i]), axis=1)
        mat[:, i] /= np.linalg.norm(mat[:, i])
    return mat[:, dim_fs:]


###################
# Subspace Angles #
###################


def subspace_angles(W1, W2):
    # Computes the subspace angles between **two orthogonal matrices**.
    # ⚠ this is not valid for non-orthogonal matrices
    return jnp.arccos(svd(W1.T @ W2, compute_uv=False))
