import sys
import time
import traceback

from exp3.workflow_utils import (
    clean_up_running_dir,
    get_case_to_run,
    report_failed,
    report_finished,
    run_case,
)


if __name__ == "__main__":
    # Creating a UID for the worker
    pbs_job_id = sys.argv[1].replace(".sched-torque.pace.gatech.edu", "")
    worker_number = sys.argv[2]
    worker_uid = "{}[{}]".format(pbs_job_id, worker_number)

    # Clean up the status directory
    try:
        clean_up_running_dir(worker_uid)
    except Exception:
        # It's okay if it fails
        pass

    # We keep going as long as a case number is provided
    while True:

        # Retrieve next case to run
        case_number = get_case_to_run(worker_uid)

        # Worker stops if no case is provided
        if case_number is None:
            # break
            time.sleep(60)  # wait for a minute to see if a new case is available

        # Run the case, report if a problem occured
        try:
            run_case(case_number, worker_uid)
            report_finished(case_number, worker_uid)
        except Exception as e:
            print(e)
            traceback.print_stack()
            report_failed(case_number, worker_uid)
