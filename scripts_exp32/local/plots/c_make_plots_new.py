from multiprocessing import Pool

import matplotlib

matplotlib.use("pdf")

from matplotlib import pyplot as plt, rc
import numpy as np
import pandas as pd
import seaborn as sns
from tqdm import tqdm

from exp3.workflow_utils import FIGURES_DIR

FONT_SIZE = 16
LEGEND_FONT_SIZE = 12
plt.rc("font", size=FONT_SIZE)  # controls default text sizes
plt.rc("axes", titlesize=FONT_SIZE)  # fontsize of the axes title
plt.rc("axes", labelsize=FONT_SIZE)  # fontsize of the x and y labels
plt.rc("xtick", labelsize=FONT_SIZE)  # fontsize of the tick labels
plt.rc("ytick", labelsize=FONT_SIZE)  # fontsize of the tick labels
plt.rc(
    "legend",
    fontsize=LEGEND_FONT_SIZE,
    title_fontsize=LEGEND_FONT_SIZE,
)  # legend items and title fontsize
plt.rc("figure", titlesize=FONT_SIZE)  # fontsize of the figure title


def make_plot(group):
    # Extract info
    name = group["name"]
    template = group["template"]
    list_dim_inputs = group["list_dim_inputs"]
    data = group["data"]

    # Retrieve lists
    num_input_dims = len(list_dim_inputs)
    fs_rel_list = ["shared", "related", "different"]
    num_fs_rel = len(fs_rel_list)

    # Initialize figure
    fig_width = num_fs_rel * 4 + 3
    fig_height = num_input_dims * 4 + 2
    fig, axes = plt.subplots(
        nrows=num_input_dims,
        ncols=num_fs_rel,
        sharex=True,
        sharey="row",
        figsize=(fig_width, fig_height),
    )
    axes = axes if isinstance(axes, np.ndarray) else np.array(axes)
    axes = np.reshape(axes, (num_input_dims, num_fs_rel))

    for j, dim_inputs in enumerate(list_dim_inputs):
        for i, fs_rel in enumerate(fs_rel_list):
            # Plot data
            ax = axes[j, i]
            sns.lineplot(
                data=data[
                    (data["dataset_pair"] == template.format(dim_inputs))
                    & (data["feature_space_relation"] == fs_rel)
                ],
                x="allocation_ratio",
                y="r2",
                hue="budget_in_hf_samples",
                ax=ax,
                legend="full",  # Make sure all labels appear
            )

            # Add grid, set x-axis limits, title, and labels
            ax.grid()
            ax.set_xlim([0.2, 0.8])
            ax.set_xlabel("LF Allocation Ratio")
            # ax.set_ylabel("Coefficient of Determination $R^2$")
            ax.set_ylabel("$R^2$")
            ax.set_title("{} inputs / {} FS".format(dim_inputs, fs_rel))

    # Redraw custom legend
    handles, labels = None, None
    for j in range(num_input_dims):
        for i in range(num_fs_rel):
            handles, labels = axes[j, i].get_legend_handles_labels()
            if axes[j, i].get_legend() is not None:
                axes[j, i].get_legend().remove()
        axes[j, -1].legend(
            handles,
            labels,
            title="Budget (in H.F.\nevaluations)",
            bbox_to_anchor=(1.04, 1.0),
            loc="upper left",
            borderaxespad=0,
            ncol=1,
        )

    # Title for the whole plot
    plt.suptitle(DATASET_NAMES[name])

    # Tight layout to fit the legend with the plot
    plt.tight_layout()

    # Save plot
    dir = FIGURES_DIR / "r2_vs_allocation_ratio_fs_rel"
    dir.mkdir(parents=True, exist_ok=True)
    # fig.savefig(dir / "{}.png".format(name), dpi=600)
    fig.savefig(dir / "{}.pdf".format(name))
    plt.close()


DATASET_NAMES = {
    "elliptic_pde_beta_0.01": "Elliptic PDE ($\\beta = 0.01$)",
    "elliptic_pde_beta_1.0": "Elliptic PDE ($\\beta = 1.0$)",
    "RAE2822_M0.725": "RAE2822 at $M=0.725$",
}


if __name__ == "__main__":
    # Retrieve data
    data = pd.read_pickle(FIGURES_DIR / "r2_vs_allocation_ratio" / "data.pkl")

    # print(data.to_string())

    # Create groups
    groups = [
        {
            "name": "elliptic_pde_beta_0.01",
            "template": "elliptic_pde_beta_0.01_modes_{}",
            "list_dim_inputs": [10, 25, 50, 100],
            "dim_fs": 3,
        },
        {
            "name": "elliptic_pde_beta_1.0",
            "template": "elliptic_pde_beta_1.0_modes_{}",
            "list_dim_inputs": [10, 25, 50, 100],
            "dim_fs": 3,
        },
    ]
    for group in groups:
        filter = [
            item in [group["template"].format(d) for d in group["list_dim_inputs"]]
            for item in data["dataset_pair"]
        ]
        group["data"] = data[filter]

    # One plot per group
    with Pool() as p:
        list(tqdm(p.imap_unordered(make_plot, groups), total=len(groups)))
