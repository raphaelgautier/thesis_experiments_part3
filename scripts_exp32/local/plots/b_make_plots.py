from multiprocessing import Pool

import matplotlib

matplotlib.use("pdf")

from matplotlib import pyplot as plt
import pandas as pd
import seaborn as sns
from tqdm import tqdm

from exp3.workflow_utils import FIGURES_DIR


def make_plot(group):
    # Extract info
    dataset_pair = group["dataset_pair"]
    output_name = group["output_name"]
    dim_fs = group["dim_fs"]
    data = group["data"]

    # Make plot
    # print(data["budget_in_hf_samples"].unique())

    fig, ax = plt.subplots()
    sns.lineplot(
        data=data,
        x="allocation_ratio",
        y="r2",
        hue="budget_in_hf_samples",
        style="feature_space_relation",
        ax=ax,
    )
    ax.grid()
    ax.set_xlim([0.0, 1.0])

    # Save plot
    fig.savefig(
        FIGURES_DIR
        / "r2_vs_allocation_ratio"
        / "{}_{}_{}D.png".format(dataset_pair, output_name, dim_fs),
        dpi=600,
    )


if __name__ == "__main__":
    # Retrieve data
    data = pd.read_pickle(FIGURES_DIR / "r2_vs_allocation_ratio" / "data.pkl")

    # Create groups
    groups = []
    for dataset_pair in data["dataset_pair"].unique():
        this_pair = data[data["dataset_pair"] == dataset_pair]
        for output_name in this_pair["output_name"].unique():
            this_output = this_pair[this_pair["output_name"] == output_name]
            for dim_fs in this_output["dim_fs"].unique():
                this_dim = this_output[this_output["dim_fs"] == dim_fs]
                groups.append(
                    {
                        "dataset_pair": dataset_pair,
                        "output_name": output_name,
                        "dim_fs": dim_fs,
                        "data": this_dim.copy(),
                    }
                )

    # One plot per group
    with Pool() as p:
        list(tqdm(p.imap_unordered(make_plot, groups), total=len(groups)))
