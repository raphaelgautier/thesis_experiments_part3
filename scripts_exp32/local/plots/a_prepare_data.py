from functools import partial
from multiprocessing import Pool
from pathlib import Path

from h5py import File
import pandas as pd
from tqdm import tqdm

from exp3.workflow_utils import DOE_DIR, FIGURES_DIR, RESULTS_DIR, DATA_DIR

def get_row(i_row, old=False):
    # Break down index from content
    i, row = i_row

    # Extract input data
    dataset_pair = row["dataset_pair"]
    output_name = row["output_name"]
    dim_fs = row["dim_feature_space"]
    budget_in_hf_samples = row["budget_in_hf_samples"]
    allocation_ratio = row["allocation_ratio"]
    lf_dataset_split_random_seed = row["lf_dataset_split_random_seed"]
    hf_dataset_split_random_seed = row["hf_dataset_split_random_seed"]
    feature_space_relation = row["feature_space_relation"]

    # Extract output data
    dir = RESULTS_DIR
    filepath = dir / "{}.h5".format(i)
    try:
        with File(filepath, "r") as h5_file:
            validation_artifacts = h5_file["validation_artifacts"]
            try:
                r2 = validation_artifacts["hf_validation"].attrs["r_squared"]
            except KeyError:
                r2 = validation_artifacts["hf_actual_vs_lf_predicted"].attrs[
                    "r_squared"
                ]

        # Return the data row
        return {
            "dataset_pair": dataset_pair,
            "output_name": output_name,
            "dim_fs": dim_fs,
            "feature_space_relation": feature_space_relation,
            "budget_in_hf_samples": budget_in_hf_samples,
            "allocation_ratio": allocation_ratio,
            "lf_dataset_split_random_seed": lf_dataset_split_random_seed,
            "hf_dataset_split_random_seed": hf_dataset_split_random_seed,
            "r2": r2,
        }
    except FileNotFoundError:
        print("{} not found".format(filepath))
        pass


if __name__ == "__main__":
    # Cases from the previous experiment
    path = DATA_DIR / "does/20211012/cases.csv"
    shared_cases = pd.read_csv(path, index_col=0)
    shared_cases = list(shared_cases.iterrows())

    with Pool() as p:
        shared_rows = list(
            tqdm(
                p.imap_unordered(partial(get_row, old=True), shared_cases),
                total=len(shared_cases),
            )
        )

    # Related / different FS cases
    cases = pd.read_csv(DOE_DIR / "cases.csv", index_col=0)
    cases = list(cases.iterrows())

    # Treat each individual results file
    with Pool() as p:
        rows = list(tqdm(p.imap_unordered(get_row, cases), total=len(cases)))

    # Concatenate rows
    all_rows = shared_rows + rows

    # Remove missing cases
    while None in all_rows:
        all_rows.remove(None)

    # print(all_rows)

    # Save extracted data
    dir = FIGURES_DIR / "r2_vs_allocation_ratio"
    # print(dir)
    dir.mkdir(parents=True, exist_ok=True)
    pd.DataFrame(all_rows).to_pickle(dir / "data.pkl")
