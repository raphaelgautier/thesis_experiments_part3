import json

import pandas as pd

from exp3.workflow_utils import DOE_DIR, STATUS_DIR, generate_doe_cases, CaseState

# DOE Parameters #######################################################################

dataset_pairs = {
    # Elliptic PDE short
    "elliptic_pde_beta_0.01_modes_10": {
        "lf": "elliptic_pde_beta_0.01_modes_10_grid_32",
        "hf": "elliptic_pde_beta_0.01_modes_10_grid_100",
        "outputs": ["y"],
        "total_cost_sweep_in_hf_samples": [4, 8, 12, 16, 20],
    },
    "elliptic_pde_beta_0.01_modes_25": {
        "lf": "elliptic_pde_beta_0.01_modes_25_grid_32",
        "hf": "elliptic_pde_beta_0.01_modes_25_grid_100",
        "outputs": ["y"],
        "total_cost_sweep_in_hf_samples": [10, 15, 20, 25, 30],
    },
    "elliptic_pde_beta_0.01_modes_50": {
        "lf": "elliptic_pde_beta_0.01_modes_50_grid_32",
        "hf": "elliptic_pde_beta_0.01_modes_50_grid_100",
        "outputs": ["y"],
        "total_cost_sweep_in_hf_samples": [20, 30, 40, 50, 60],
    },
    "elliptic_pde_beta_0.01_modes_100": {
        "lf": "elliptic_pde_beta_0.01_modes_100_grid_32",
        "hf": "elliptic_pde_beta_0.01_modes_100_grid_100",
        "outputs": ["y"],
        "total_cost_sweep_in_hf_samples": [70, 80, 90, 100, 110],
    },
    # Elliptic PDE long
    "elliptic_pde_beta_1.0_modes_10": {
        "lf": "elliptic_pde_beta_1.0_modes_10_grid_32",
        "hf": "elliptic_pde_beta_1.0_modes_10_grid_100",
        "outputs": ["y"],
        "total_cost_sweep_in_hf_samples": [4, 8, 12, 16, 20],
    },
    "elliptic_pde_beta_1.0_modes_25": {
        "lf": "elliptic_pde_beta_1.0_modes_25_grid_32",
        "hf": "elliptic_pde_beta_1.0_modes_25_grid_100",
        "outputs": ["y"],
        "total_cost_sweep_in_hf_samples": [10, 15, 20, 25, 30],
    },
    "elliptic_pde_beta_1.0_modes_50": {
        "lf": "elliptic_pde_beta_1.0_modes_50_grid_32",
        "hf": "elliptic_pde_beta_1.0_modes_50_grid_100",
        "outputs": ["y"],
        "total_cost_sweep_in_hf_samples": [20, 30, 40, 50, 60],
    },
    "elliptic_pde_beta_1.0_modes_100": {
        "lf": "elliptic_pde_beta_1.0_modes_100_grid_32",
        "hf": "elliptic_pde_beta_1.0_modes_100_grid_100",
        "outputs": ["y"],
        "total_cost_sweep_in_hf_samples": [70, 80, 90, 100, 110],
    },
}

# DOE ranges
alt_allocation_ratios = [0, 0.2, 0.4, 0.6, 0.8, 1.0]
alt_dataset_split_random_seeds = [
    (867, 25),
    (57, 654),
    (353, 951),
    (601, 265),
    (802, 758),
]
alt_feature_space_relation = ["different", "related"]
alt_dim_fs = [1, 3, 5]

# Process Paramaters ###################################################################

PROCESS_PARAMETERS = {
    "training_parameters": {
        "mcmc_parameters": {
            "target_acceptance_probability": 0.8,
            "num_warmup_draws": 500,
            "num_posterior_draws": 1000,
            "random_seed": 0,
            "num_draws_between_saves": 50,
            "progress_bar": False,
            "display_summary": False,
        }
    },
    "validation_parameters": {
        "num_lf_predictions": 15,
        "lf_predictions_seed": 867,
        "confidence_interval_bounds_cdf_values": [0.025, 0.975],
        "validation_predictions_random_seed": 562,
        "num_gp_samples": 10,
    },
}

# Generate the DOE #####################################################################

cases = generate_doe_cases(
    dataset_pairs,
    alt_allocation_ratios,
    alt_dataset_split_random_seeds,
    alt_feature_space_relation,
    alt_dim_fs,
)

# Initialize the DOE folder
DOE_DIR.mkdir(parents=True, exist_ok=True)

# Subfolders
for subfolder in ["figures", "results", "status", "temp"]:
    (DOE_DIR / subfolder).mkdir(parents=True, exist_ok=True)

# Create directories for each possible state
for state in [state.value for state in CaseState]:
    (STATUS_DIR / state).mkdir(parents=True, exist_ok=True)

# Initially add all DOE cases in UNSTARTED
for case_number in range(len(cases)):
    (STATUS_DIR / CaseState.UNSTARTED.value / str(case_number)).touch()

# Save the DOE files

# Process parameters
with open(DOE_DIR / "process_parameters.json", "w") as process_parameters_file:
    json.dump(PROCESS_PARAMETERS, process_parameters_file, indent=2)

# Cases in a CSV
pd.DataFrame(cases).to_csv(DOE_DIR / "cases.csv")
