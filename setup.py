from setuptools import setup

setup(
    name="ThesisExperimentsPart3",
    version="0.0.1",
    url="https://gitlab.com/raphaelgautier/thesis_experiments_part3",
    author="Raphaël Gautier",
    author_email="raphael.gautier@gatech.edu",
    description="Third part of the code implementing thesis experiments.",
    packages=["exp3"],
    install_requires=[
        "numpy",
        "h5py",
        "pandas",
        "jax",
        "matplotlib",
        "scipy",
        "tqdm",
        "numpyro",
        "seaborn",
        "dill",
    ],
    python_requires="==3.7.*",
)
