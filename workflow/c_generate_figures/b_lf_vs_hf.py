from multiprocessing import Pool

import matplotlib

matplotlib.use("pdf")

from matplotlib import pyplot as plt
import numpy as np
import pandas as pd
from tqdm import tqdm

from exp3.workflow_utils import (
    load_dataset,
    FIGURES_DIR,
    rmse_nrmse_r_squared_and_absolute_error,
)

# List all available dataset pairs
DATASET_PAIRS = {
    # Elliptic PDE short
    "elliptic_pde_beta_0.01_modes_10": {
        "lf": "elliptic_pde_beta_0.01_modes_10_grid_32",
        "hf": "elliptic_pde_beta_0.01_modes_10_grid_100",
        "outputs": ["y"],
    },
    "elliptic_pde_beta_0.01_modes_25": {
        "lf": "elliptic_pde_beta_0.01_modes_25_grid_32",
        "hf": "elliptic_pde_beta_0.01_modes_25_grid_100",
        "outputs": ["y"],
    },
    "elliptic_pde_beta_0.01_modes_50": {
        "lf": "elliptic_pde_beta_0.01_modes_50_grid_32",
        "hf": "elliptic_pde_beta_0.01_modes_50_grid_100",
        "outputs": ["y"],
    },
    "elliptic_pde_beta_0.01_modes_100": {
        "lf": "elliptic_pde_beta_0.01_modes_100_grid_32",
        "hf": "elliptic_pde_beta_0.01_modes_100_grid_100",
        "outputs": ["y"],
    },
    # Elliptic PDE long
    "elliptic_pde_beta_1.0_modes_10": {
        "lf": "elliptic_pde_beta_1.0_modes_10_grid_32",
        "hf": "elliptic_pde_beta_1.0_modes_10_grid_100",
        "outputs": ["y"],
    },
    "elliptic_pde_beta_1.0_modes_25": {
        "lf": "elliptic_pde_beta_1.0_modes_25_grid_32",
        "hf": "elliptic_pde_beta_1.0_modes_25_grid_100",
        "outputs": ["y"],
    },
    "elliptic_pde_beta_1.0_modes_50": {
        "lf": "elliptic_pde_beta_1.0_modes_50_grid_32",
        "hf": "elliptic_pde_beta_1.0_modes_50_grid_100",
        "outputs": ["y"],
    },
    "elliptic_pde_beta_1.0_modes_100": {
        "lf": "elliptic_pde_beta_1.0_modes_100_grid_32",
        "hf": "elliptic_pde_beta_1.0_modes_100_grid_100",
        "outputs": ["y"],
    },
    # RAE
    "RAE2822_M0.725_15DV": {
        "lf": "RAE2822_M0.725_15DV_11k",
        "hf": "RAE2822_M0.725_15DV_baseline",
        "outputs": ["lift"],
    },
    "RAE2822_M0.725_25DV": {
        "lf": "RAE2822_M0.725_25DV_11k",
        "hf": "RAE2822_M0.725_25DV_baseline",
        "outputs": ["lift"],
    },
    "RAE2822_M0.725_51DV": {
        "lf": "RAE2822_M0.725_51DV_11k",
        "hf": "RAE2822_M0.725_51DV_baseline",
        "outputs": ["lift"],
    },
}

PRETTY_PAIR_NAMES = {
    "elliptic_pde_beta_0.01_modes_10": "Elliptic PDE ($\\beta = 0.01$, 10 modes)",
    "elliptic_pde_beta_0.01_modes_25": "Elliptic PDE ($\\beta = 0.01$, 25 modes)",
    "elliptic_pde_beta_0.01_modes_50": "Elliptic PDE ($\\beta = 0.01$, 50 modes)",
    "elliptic_pde_beta_0.01_modes_100": "Elliptic PDE ($\\beta = 0.01$, 100 modes)",
    "elliptic_pde_beta_1.0_modes_10": "Elliptic PDE ($\\beta = 1.0$, 10 modes)",
    "elliptic_pde_beta_1.0_modes_25": "Elliptic PDE ($\\beta = 1.0$, 25 modes)",
    "elliptic_pde_beta_1.0_modes_50": "Elliptic PDE ($\\beta = 1.0$, 50 modes)",
    "elliptic_pde_beta_1.0_modes_100": "Elliptic PDE ($\\beta = 1.0$, 100 modes)",
    "RAE2822_M0.725_15DV": "RAE2822 at $M=0.725$ (15D inputs)",
    "RAE2822_M0.725_25DV": "RAE2822 at $M=0.725$ (25D inputs)",
    "RAE2822_M0.725_51DV": "RAE2822 at $M=0.725$ (51D inputs)",
}


def plot_actual_vs_predicted(
    actual_y,
    predicted_y,
    title="Actual vs. Predicted",
    xlabel="Actual",
    ylabel="Predicted",
):
    # Retrieve a figure
    fig, ax = plt.subplots(1, 1, figsize=(5, 5))

    # Compute min and max for settings plot bounds
    mmin = min(np.min(actual_y), np.min(predicted_y))
    mmax = max(np.max(actual_y), np.max(predicted_y))
    padding = (mmax - mmin) * 0.1
    bounds = [mmin - padding, mmax + padding]

    # 45-degree line
    ax.plot(bounds, bounds, "--")

    # Scatter plot of the data
    print(title, actual_y.shape, predicted_y.shape)
    ax.scatter(actual_y, predicted_y, alpha=0.2)

    # Compute R2
    _, _, r_squared, _ = rmse_nrmse_r_squared_and_absolute_error(
        predicted_y.to_numpy(), actual_y.to_numpy()
    )

    # Plot title and labels
    if title is not None:
        ax.set_title(title + "\n$R^2 = {:.2f}$".format(r_squared))
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)

    # Ensure the plotting area is square with a grid
    ax.set_aspect(1.0)
    ax.set_xlim(bounds)
    ax.set_ylim(bounds)
    ax.grid()

    # Tight layout
    plt.tight_layout()

    return fig, ax


def create_plot(dataset_name_and_pair):
    # Retrieve pair name and content
    pair_name = dataset_name_and_pair[0]
    dataset_pair = dataset_name_and_pair[1]

    # One plot per output
    for output_name in dataset_pair["outputs"]:
        lf_x, lf_y, _ = load_dataset(dataset_pair["lf"], output_name)
        hf_x, hf_y, _ = load_dataset(dataset_pair["hf"], output_name)

        # Using pandas to make a join
        inputs = ["x_{}".format(i) for i in range(lf_x.shape[1])]
        lf_dataset = pd.DataFrame(
            data=np.concatenate((lf_x, lf_y.reshape(-1, 1)), axis=1),
            columns=inputs + ["lf_y"],
        )
        hf_dataset = pd.DataFrame(
            data=np.concatenate((hf_x, hf_y.reshape(-1, 1)), axis=1),
            columns=inputs + ["hf_y"],
        )
        mf_dataset = pd.merge(lf_dataset, hf_dataset, how="inner", on=inputs)

        fig, _ = plot_actual_vs_predicted(
            mf_dataset["hf_y"],
            mf_dataset["lf_y"],
            xlabel="High-Fidelity Samples",
            ylabel="Low-Fidelity Samples",
            title=PRETTY_PAIR_NAMES[pair_name],
        )
        fig.savefig(
            FIGURES_DIR / "lf_vs_hf" / "{}_{}.pdf".format(pair_name, output_name),
            # FIGURES_DIR / "lf_vs_hf" / "{}_{}.png".format(pair_name, output_name),
            # dpi=600,
        )
    pass


# One plot per dataset_pair
items = DATASET_PAIRS.items()
with Pool() as p:
    list(tqdm(p.imap_unordered(create_plot, items), total=len(items)))
