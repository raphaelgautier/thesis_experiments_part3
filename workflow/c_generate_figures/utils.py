from pathlib import Path

import h5py
from matplotlib import use

use("pdf")
from matplotlib import pyplot as plt
import numpy as np

from exp3.workflow_utils import get_data_dir, load_dataset, CaseType


def plot_actual_vs_predicted_asymmetric(
    actual_y,
    predicted_y,
    training_confidence_interval_bounds,
    title=None,
    xlabel="Actual",
    ylabel="Predicted",
    ax=None,
):
    errors = np.concatenate(
        (
            predicted_y - training_confidence_interval_bounds[:, 0:1],
            training_confidence_interval_bounds[:, 1:2] - predicted_y,
        ),
        axis=1,
    ).T

    mmin = min(np.min(actual_y), np.min(predicted_y))
    mmax = max(np.max(actual_y), np.max(predicted_y))
    padding = (mmax - mmin) * 0.1
    bounds = [mmin - padding, mmax + padding]

    if ax is None:
        fig, ax = plt.subplots(1, 1)

    ax.errorbar(actual_y, predicted_y, yerr=errors, fmt="o", alpha=0.2)

    ax.set_title("Predicted vs. Actual (Training)")
    ax.plot(bounds, bounds, "--")
    ax.set_aspect(1.0)
    ax.set_xlim(bounds)
    ax.set_ylim(bounds)
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)

    plt.grid()
    if title is not None:
        plt.title(title)
    plt.tight_layout()
    # plt.show()
    # plt.savefig('figures/training_actual_vs_predicted')

    return fig


def plot_actual_vs_predicted_helper(
    doe_name, dataset_pair, output_name, case_name, y_actual, h5_file, val_type
):

    point_based_predictions = h5_file["validation_artifacts"][val_type][
        "point_based_predictions"
    ]
    confidence_interval_bounds = h5_file["validation_artifacts"][val_type][
        "confidence_interval_bounds"
    ]
    r_squared = h5_file["validation_artifacts"][val_type].attrs["r_squared"]

    title = "{} - {}\n$R^2 = {:.2f}$".format(case_name, type, r_squared)
    print(title)

    # Output dir
    out_dir = (
        get_data_dir()
        / "figures"
        / "actual_vs_predicted"
        / doe_name
        / dataset_pair
        / output_name
    )
    out_dir.mkdir(parents=True, exist_ok=True)

    # Plot
    fig = plot_actual_vs_predicted_asymmetric(
        y_actual,
        point_based_predictions,
        confidence_interval_bounds,
        title=title,
    )
    print("{case_name}_{val_type}.pdf")
    plt.savefig(out_dir / f"{case_name}_{val_type}.pdf")
    plt.close(fig)


def generate_actual_vs_predicted_plots(results_filepath):
    print(results_filepath)
    # Load relevant info from the results file
    with h5py.File(results_filepath, "r") as h5_file:

        # Shortcut
        case = h5_file["case_parameters"].attrs

        # Figuring it out the case type
        num_lf_training_samples = case["num_lf_training_samples"]
        num_hf_training_samples = case["num_hf_training_samples"]
        case_type = (
            CaseType.HF_ONLY
            if num_lf_training_samples == 0
            else CaseType.LF_ONLY
            if num_hf_training_samples == 0
            else CaseType.MULTI_FIDELITY
        )

        # Common
        doe_name = h5_file.attrs["doe_name"]
        dataset_pair = case["dataset_pair"]
        output_name = case["output_name"]
        case_name = Path(results_filepath).stem
        _, lf_y, _ = load_dataset(case["lf_dataset_name"], output_name)
        _, hf_y, _ = load_dataset(case["hf_dataset_name"], output_name)

        if case_type in [CaseType.LF_ONLY, CaseType.MULTI_FIDELITY]:
            # LF training
            y_actual = lf_y[h5_file["training_artifacts"]["training_indices"]["lf"]]
            val_type = "lf_training"
            plot_actual_vs_predicted_helper(
                doe_name,
                dataset_pair,
                output_name,
                case_name,
                y_actual,
                h5_file,
                val_type,
            )

            # LF validation
            y_actual = lf_y[h5_file["validation_artifacts"]["validation_indices"]["lf"]]
            val_type = "lf_validation"
            plot_actual_vs_predicted_helper(
                doe_name,
                dataset_pair,
                output_name,
                case_name,
                y_actual,
                h5_file,
                val_type,
            )

            # HF actual vs. LF predicted at HF validation points
            y_actual = hf_y[h5_file["validation_artifacts"]["validation_indices"]["hf"]]
            val_type = "hf_actual_vs_lf_predicted"
            plot_actual_vs_predicted_helper(
                doe_name,
                dataset_pair,
                output_name,
                case_name,
                y_actual,
                h5_file,
                val_type,
            )

        if case_type in [CaseType.HF_ONLY, CaseType.MULTI_FIDELITY]:
            # HF training
            y_actual = hf_y[h5_file["training_artifacts"]["training_indices"]["hf"]]
            val_type = "hf_training"
            plot_actual_vs_predicted_helper(
                doe_name,
                dataset_pair,
                output_name,
                case_name,
                y_actual,
                h5_file,
                val_type,
            )

            # HF validation
            y_actual = hf_y[h5_file["validation_artifacts"]["validation_indices"]["hf"]]
            val_type = "hf_validation"
            plot_actual_vs_predicted_helper(
                doe_name,
                dataset_pair,
                output_name,
                case_name,
                y_actual,
                h5_file,
                val_type,
            )
