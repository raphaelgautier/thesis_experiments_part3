from multiprocessing import Pool

from h5py import File
import pandas as pd
from tqdm import tqdm

from exp3.workflow_utils import DOE_DIR, FIGURES_DIR, RESULTS_DIR

"""
TODO
"""


def get_row(i_row):
    # Break down index from content
    i, row = i_row

    # Extract input data
    dataset_pair = row["dataset_pair"]
    output_name = row["output_name"]
    dim_fs = row["dim_feature_space"]
    budget_in_hf_samples = row["budget_in_hf_samples"]
    allocation_ratio = row["allocation_ratio"]
    lf_dataset_split_random_seed = row["lf_dataset_split_random_seed"]
    hf_dataset_split_random_seed = row["hf_dataset_split_random_seed"]

    # Extract output data
    try:
        with File(RESULTS_DIR / "{}.h5".format(i), "r") as h5_file:
            validation_artifacts = h5_file["validation_artifacts"]
            try:
                r2 = validation_artifacts["hf_validation"].attrs["r_squared"]
            except KeyError:
                r2 = validation_artifacts["hf_actual_vs_lf_predicted"].attrs[
                    "r_squared"
                ]

        # Return the data row
        return {
            "dataset_pair": dataset_pair,
            "output_name": output_name,
            "dim_fs": dim_fs,
            "budget_in_hf_samples": budget_in_hf_samples,
            "allocation_ratio": allocation_ratio,
            "lf_dataset_split_random_seed": lf_dataset_split_random_seed,
            "hf_dataset_split_random_seed": hf_dataset_split_random_seed,
            "r2": r2,
        }
    except FileNotFoundError:
        pass


if __name__ == "__main__":
    # Filter cases with shared feature space
    cases = pd.read_csv(DOE_DIR / "cases.csv", index_col=0)
    shared_cases = cases[cases["feature_space_relation"] == "shared"]
    shared_cases = list(shared_cases.iterrows())

    # Treat each individual results file
    with Pool() as p:
        rows = list(
            tqdm(p.imap_unordered(get_row, shared_cases), total=len(shared_cases))
        )

    # Remove missing cases
    while None in rows:
        rows.remove(None)

    # Save extracted data
    dir = FIGURES_DIR / "r2_vs_allocation_ratio"
    dir.mkdir(parents=True, exist_ok=True)
    pd.DataFrame(rows).to_pickle(dir / "data.pkl")
