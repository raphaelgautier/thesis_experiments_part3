from multiprocessing import Pool

import matplotlib

matplotlib.use("pdf")

from matplotlib import pyplot as plt, rc
import numpy as np
import pandas as pd
import seaborn as sns
from tqdm import tqdm

from exp3.workflow_utils import FIGURES_DIR

FONT_SIZE = 16
LEGEND_FONT_SIZE = 12
plt.rc("font", size=FONT_SIZE)  # controls default text sizes
plt.rc("axes", titlesize=FONT_SIZE)  # fontsize of the axes title
plt.rc("axes", labelsize=FONT_SIZE)  # fontsize of the x and y labels
plt.rc("xtick", labelsize=FONT_SIZE)  # fontsize of the tick labels
plt.rc("ytick", labelsize=FONT_SIZE)  # fontsize of the tick labels
plt.rc(
    "legend",
    fontsize=LEGEND_FONT_SIZE,
    title_fontsize=LEGEND_FONT_SIZE,
)  # legend items and title fontsize
plt.rc("figure", titlesize=FONT_SIZE)  # fontsize of the figure title


def make_plot(group):
    # Extract info
    name = group["name"]
    template = group["template"]
    list_dim_inputs = group["list_dim_inputs"]
    data = group["data"]

    # Retrieve lists
    num_input_dims = len(list_dim_inputs)
    list_dims_fs = sorted(list(data["dim_fs"].unique()))
    num_fs_dims = len(list_dims_fs)

    # Initialize figure
    fig_width = num_fs_dims * 4 + 3
    fig_height = num_input_dims * 4
    fig, axes = plt.subplots(
        nrows=num_input_dims,
        ncols=num_fs_dims,
        sharex=True,
        sharey="row",
        figsize=(fig_width, fig_height),
    )
    axes = axes if isinstance(axes, np.ndarray) else np.array(axes)
    axes = np.reshape(axes, (num_input_dims, num_fs_dims))

    for j, dim_inputs in enumerate(list_dim_inputs):
        for i, dim_fs in enumerate(list_dims_fs):
            # Plot data
            ax = axes[j, i]
            sns.lineplot(
                data=data[
                    (data["dataset_pair"] == template.format(dim_inputs))
                    & (data["dim_fs"] == dim_fs)
                ],
                x="allocation_ratio",
                y="r2",
                hue="budget_in_hf_samples",
                ax=ax,
                legend="full",  # Make sure all labels appear
            )

            # Add grid, set x-axis limits, title, and labels
            ax.grid()
            ax.set_xlim([0.0, 1.0])
            ax.set_xlabel("LF Allocation Ratio")
            # ax.set_ylabel("Coefficient of Determination $R^2$")
            ax.set_ylabel("$R^2$")
            ax.set_title("{} inputs / {}D FS".format(dim_inputs, dim_fs))

    # Redraw custom legend
    handles, labels = None, None
    for j in range(num_input_dims):
        for i in range(num_fs_dims):
            handles, labels = axes[j, i].get_legend_handles_labels()
            axes[j, i].get_legend().remove()
        axes[j, -1].legend(
            handles,
            labels,
            title="Budget (in H.F.\nevaluations)",
            bbox_to_anchor=(1.04, 1.0),
            loc="upper left",
            borderaxespad=0,
            ncol=1,
        )

    # Title for the whole plot
    plt.suptitle(DATASET_NAMES[name])

    # Tight layout to fit the legend with the plot
    plt.tight_layout()

    # Save plot
    dir = FIGURES_DIR / "r2_vs_allocation_ratio_per_dim"
    dir.mkdir(parents=True, exist_ok=True)
    # fig.savefig(dir / "{}.png".format(name), dpi=600)
    fig.savefig(dir / "{}.pdf".format(name))
    plt.close()


# Pretty names for the datasets
# DATASET_NAMES = {
#     "elliptic_pde_beta_0.01_modes_10": "Elliptic PDE ($\\beta = 0.01$, 10 modes)",
#     "elliptic_pde_beta_0.01_modes_25": "Elliptic PDE ($\\beta = 0.01$, 25 modes)",
#     "elliptic_pde_beta_0.01_modes_50": "Elliptic PDE ($\\beta = 0.01$, 50 modes)",
#     "elliptic_pde_beta_0.01_modes_100": "Elliptic PDE ($\\beta = 0.01$, 100 modes)",
#     "elliptic_pde_beta_1.0_modes_10": "Elliptic PDE ($\\beta = 1.0$, 10 modes)",
#     "elliptic_pde_beta_1.0_modes_25": "Elliptic PDE ($\\beta = 1.0$, 25 modes)",
#     "elliptic_pde_beta_1.0_modes_50": "Elliptic PDE ($\\beta = 1.0$, 50 modes)",
#     "elliptic_pde_beta_1.0_modes_100": "Elliptic PDE ($\\beta = 1.0$, 100 modes)",
#     "RAE2822_M0.725_15DV": "RAE2822 at $M=0.725$ (15D inputs)",
#     "RAE2822_M0.725_25DV": "RAE2822 at $M=0.725$ (25D inputs)",
#     "RAE2822_M0.725_51DV": "RAE2822 at $M=0.725$ (51D inputs)",
# }

DATASET_NAMES = {
    "elliptic_pde_beta_0.01": "Elliptic PDE ($\\beta = 0.01$)",
    "elliptic_pde_beta_1.0": "Elliptic PDE ($\\beta = 1.0$)",
    "RAE2822_M0.725": "RAE2822 at $M=0.725$",
}


if __name__ == "__main__":
    # Retrieve data
    data = pd.read_pickle(FIGURES_DIR / "r2_vs_allocation_ratio" / "data.pkl")

    # Create groups
    groups = [
        {
            "name": "elliptic_pde_beta_0.01",
            "template": "elliptic_pde_beta_0.01_modes_{}",
            "list_dim_inputs": [10, 25, 50, 100],
        },
        {
            "name": "elliptic_pde_beta_1.0",
            "template": "elliptic_pde_beta_1.0_modes_{}",
            "list_dim_inputs": [10, 25, 50, 100],
        },
        {
            "name": "RAE2822_M0.725",
            "template": "RAE2822_M0.725_{}DV",
            "list_dim_inputs": [15, 25, 51],
        },
    ]
    for group in groups:
        filter = [
            item in [group["template"].format(d) for d in group["list_dim_inputs"]]
            for item in data["dataset_pair"]
        ]
        group["data"] = data[filter]

    # One plot per group
    with Pool() as p:
        list(tqdm(p.imap_unordered(make_plot, groups), total=len(groups)))
