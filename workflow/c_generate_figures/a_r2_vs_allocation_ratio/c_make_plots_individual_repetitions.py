from multiprocessing import Pool

import matplotlib

matplotlib.use("pdf")

from matplotlib import pyplot as plt
from matplotlib.lines import Line2D
import pandas as pd
import seaborn as sns
from tqdm import tqdm

from exp3.workflow_utils import FIGURES_DIR

COLORS = plt.rcParams["axes.prop_cycle"].by_key()["color"]


def make_plot(group):
    # Extract info
    dataset_pair = group["dataset_pair"]
    output_name = group["output_name"]
    dim_fs = group["dim_fs"]
    data = group["data"]

    # Make plot
    budgets = data["budget_in_hf_samples"].unique()
    lines = {}
    fig, ax = plt.subplots()
    for i, budget in enumerate(budgets):
        this_budget = data[data["budget_in_hf_samples"] == budget]
        for repetition in this_budget["lf_dataset_split_random_seed"].unique():
            this_repetition = this_budget[
                this_budget["lf_dataset_split_random_seed"] == repetition
            ].sort_values(by="allocation_ratio")
            (lines[i],) = ax.plot(
                this_repetition["allocation_ratio"],
                this_repetition["r2"],
                color=COLORS[i],
            )

    # Grid and x-limits
    ax.grid()
    ax.set_xlim([0.0, 1.0])

    # # Legend
    ax.legend(
        handles=[
            Line2D([], [], color=COLORS[i], label="{}".format(budget))
            for i, budget in enumerate(budgets)
        ]
    )

    # Save plot
    dir = FIGURES_DIR / "r2_vs_allocation_ratio_individual_repetitions"
    dir.mkdir(parents=True, exist_ok=True)
    fig.savefig(
        dir / "{}_{}_{}D.png".format(dataset_pair, output_name, dim_fs),
        dpi=600,
    )


if __name__ == "__main__":
    # Retrieve data
    data = pd.read_pickle(FIGURES_DIR / "r2_vs_allocation_ratio" / "data.pkl")

    # Create groups
    groups = []
    for dataset_pair in data["dataset_pair"].unique():
        this_pair = data[data["dataset_pair"] == dataset_pair]
        for output_name in this_pair["output_name"].unique():
            this_output = this_pair[this_pair["output_name"] == output_name]
            for dim_fs in this_output["dim_fs"].unique():
                this_dim = this_output[this_output["dim_fs"] == dim_fs]
                groups.append(
                    {
                        "dataset_pair": dataset_pair,
                        "output_name": output_name,
                        "dim_fs": dim_fs,
                        "data": this_dim.copy(),
                    }
                )

    # One plot per group
    with Pool() as p:
        list(tqdm(p.imap_unordered(make_plot, groups), total=len(groups)))
