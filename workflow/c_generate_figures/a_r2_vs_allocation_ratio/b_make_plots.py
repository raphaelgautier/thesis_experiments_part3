from multiprocessing import Pool

import matplotlib

matplotlib.use("pdf")

from matplotlib import pyplot as plt
import pandas as pd
import seaborn as sns
from tqdm import tqdm

from exp3.workflow_utils import FIGURES_DIR

# Pretty names for the datasets
DATASET_NAMES = {
    "elliptic_pde_beta_0.01_modes_10": "Elliptic PDE ($\\beta = 0.01$, 10 modes)",
    "elliptic_pde_beta_0.01_modes_25": "Elliptic PDE ($\\beta = 0.01$, 25 modes)",
    "elliptic_pde_beta_0.01_modes_50": "Elliptic PDE ($\\beta = 0.01$, 50 modes)",
    "elliptic_pde_beta_0.01_modes_100": "Elliptic PDE ($\\beta = 0.01$, 100 modes)",
    "elliptic_pde_beta_1.0_modes_10": "Elliptic PDE ($\\beta = 1.0$, 10 modes)",
    "elliptic_pde_beta_1.0_modes_25": "Elliptic PDE ($\\beta = 1.0$, 25 modes)",
    "elliptic_pde_beta_1.0_modes_50": "Elliptic PDE ($\\beta = 1.0$, 50 modes)",
    "elliptic_pde_beta_1.0_modes_100": "Elliptic PDE ($\\beta = 1.0$, 100 modes)",
    "RAE2822_M0.725_15DV": "RAE2822 at $M=0.725$ (15D inputs)",
    "RAE2822_M0.725_25DV": "RAE2822 at $M=0.725$ (25D inputs)",
    "RAE2822_M0.725_51DV": "RAE2822 at $M=0.725$ (51D inputs)",
}


def make_plot(group):
    # Extract info
    dataset_pair = group["dataset_pair"]
    output_name = group["output_name"]
    dim_fs = group["dim_fs"]
    data = group["data"]

    # Initialize figure
    fig, ax = plt.subplots(figsize=(8, 5))

    # Plot data
    sns.lineplot(
        data=data,
        x="allocation_ratio",
        y="r2",
        hue="budget_in_hf_samples",
        ax=ax,
        legend="full",  # Make sure all labels appear
    )

    # Add grid, set x-axis limits, title, and labels
    ax.grid()
    ax.set_xlim([0.0, 1.0])
    ax.set_xlabel("Low-Fidelity Allocation Ratio")
    ax.set_ylabel("Coefficient of Determination $R^2$")
    ax.set_title(
        "{}\nOutput {} - {}D Feature Space".format(
            DATASET_NAMES[group["dataset_pair"]], group["output_name"], group["dim_fs"]
        )
    )

    # Redraw custom legend
    legend = ax.get_legend()
    handles, labels = ax.get_legend_handles_labels()
    #  = legend.legendHandles
    # legend_labels = legend.legendLabels
    legend.remove()
    ax.legend(
        handles,
        labels,
        title="Budget (in H.F.\nevaluations)",
        bbox_to_anchor=(1.04, 1.0),
        loc="upper left",
        borderaxespad=0,
        ncol=1,
    )

    # Tight layout to fit the legend with the plot
    plt.tight_layout()

    # Save plot
    fig.savefig(
        FIGURES_DIR
        / "r2_vs_allocation_ratio"
        / "{}_{}_{}D.pdf".format(dataset_pair, output_name, dim_fs),
        dpi=600,
    )
    plt.close()


if __name__ == "__main__":
    # Retrieve data
    data = pd.read_pickle(FIGURES_DIR / "r2_vs_allocation_ratio" / "data.pkl")

    # Create groups
    groups = []
    for dataset_pair in data["dataset_pair"].unique():
        this_pair = data[data["dataset_pair"] == dataset_pair]
        for output_name in this_pair["output_name"].unique():
            this_output = this_pair[this_pair["output_name"] == output_name]
            for dim_fs in this_output["dim_fs"].unique():
                this_dim = this_output[this_output["dim_fs"] == dim_fs]
                groups.append(
                    {
                        "dataset_pair": dataset_pair,
                        "output_name": output_name,
                        "dim_fs": dim_fs,
                        "data": this_dim.copy(),
                    }
                )

    from pprint import pprint

    pprint(groups)

    # One plot per group
    with Pool() as p:
        list(tqdm(p.imap_unordered(make_plot, groups), total=len(groups)))
